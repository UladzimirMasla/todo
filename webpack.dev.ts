import path from 'path';
import webpack from "webpack";
import { Configuration as WebpackDevServerConfiguration } from "webpack-dev-server";
import { merge } from 'webpack-merge';
import CommonConfiguration from './webpack.common';


interface Configuration extends webpack.Configuration {
  devServer?: WebpackDevServerConfiguration;
}

const devConfiguration : Configuration = merge(CommonConfiguration as Configuration, {
    mode: 'development',
    devtool: 'eval-source-map',
    module: {
      rules: [
        {
          test: /\.(scss|css)$/,
          use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
        },
      ]
    },
    devServer: {
        historyApiFallback: true,
        open: true,
        compress: true,
        hot: true,
        port: 8080,
        static: './public'
      },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
    ],
    optimization: {
      moduleIds: 'named'
    }
});

export default devConfiguration;