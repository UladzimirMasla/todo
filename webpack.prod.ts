import path from 'path';
import webpack from "webpack";
import { merge } from 'webpack-merge';
import CommonConfiguration from './webpack.common';
import TerserPlugin from 'terser-webpack-plugin';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin';

module.exports = merge(CommonConfiguration, {
    mode: 'production',
    output: {
        filename: '[name].[chunkhash].js',
        path: path.resolve(__dirname, './dist'),
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.(scss|css)$/,
                use: [
                  MiniCssExtractPlugin.loader,
                  {
                    loader: 'css-loader',
                    options: {
                      importLoaders: 2,
                      sourceMap: false,
                      modules: true,
                    },
                  },
                  'postcss-loader',
                  'sass-loader',
                ],
            },
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        // Extracts CSS into separate files
        new MiniCssExtractPlugin({
            filename: 'styles/[name].[contenthash].css',
            chunkFilename: '[id].css',
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production'),
        }),
        new webpack.ids.HashedModuleIdsPlugin(),
    ],
    optimization: {
        minimize: true,
        minimizer: [new TerserPlugin({ parallel: true }), new CssMinimizerPlugin(), '...'],
      },
});
