import path from 'path';
import webpack from "webpack";
import CopyWebpackPlugin from 'copy-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';

const config: webpack.Configuration = {
    entry: {
      main: "./src/index.js",
      scripts: "./src/Common/index.js"
    },
    output: {
      path: path.resolve(__dirname, "dist/"),
      publicPath: "/",
      filename: "[name].bundle.js"
    },
    resolve: {
      extensions: ["*", ".ts", ".tsx", ".js"]
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /(node_modules|bower_components)/,
          loader: "babel-loader",
          options: { presets: ["@babel/env"] }
        },
        {
          test: /\.(ts|tsx)$/,
          exclude: /node_modules/,
          use: ["ts-loader"],
        },
        {
          test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
          type: 'asset/resource',
        },
        {
          test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
          type: 'asset/inline',
        }
      ]
    },
    plugins: [
      new webpack.DefinePlugin({
        APPLICATION_VERSION: JSON.stringify('1.0.0'),
    }),
    new HtmlWebpackPlugin({
      title: 'Webpack Boilerplate',
      template: path.resolve(__dirname, './src/template.html'),
      filename: 'index.html',
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, './public'),
          to: 'assets',
          noErrorOnMissing: true,
        },
      ],
    }),
    ]
  };

  export default config;