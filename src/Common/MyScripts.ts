

class MyClass
{
    private some: string;

    constructor(input: string) {
        this.some = input;
    }

    getSome(): string {

        return this.some;
    }
}

export { MyClass };